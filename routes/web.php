<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'TasksController@index')->name('home');
Route::get('/task','TasksController@create')->name('add-task');
Route::post('/task','TasksController@store')->name('create-new-task');

Route::get('/task/{task}','TasksController@edit');
Route::post('/task/{id}','TasksController@update');
Route::delete('/task/{id}','TasksController@destroy')->name('delete-task');
Route::put('/task/{id}','TasksController@completeTask')->name('complete-task');
