<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        return view('tasks.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $task = null;
        return view('tasks.add',compact('task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     try{
         $data = $request->all();
         $data['user_id']= Auth::id();
         Task::create($data);
         return redirect()->back()->with(['success'=>'Task added successfully']);
     }catch (\Exception $e){
         return Redirect::back()->withErrors($e->getMessage())->withInput();
     }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $task = Task::findOrFail($id);
        if (Auth::check() && Auth::user()->id == $task->user_id)
        {
            return view('tasks.edit', compact('task'));
        }
        else {
            return Redirect::back()->withErrors("Unauthorised")->withInput();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, $id)
    {
        //
        try{
            if($request->validated()){
                $task = Task::findOrFail($id);
                $task->name = $request->get('name');
                $task->description = $request->get('description');
                $task->save();
                return redirect()->back()->with(['success'=>'Task Updated successfully']);
            }
        }catch (\Exception $e){
            return Redirect::back()->withErrors($e->getMessage())->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $task = Task::findOrFail($id);
        $task->delete();
        return redirect()->back()->with(['success'=>'Task deleted successfully']);
    }
    public function completeTask($id){
        $task = Task::findOrFail($id);
        if(!$task->is_complete){
            $task->is_complete  =1;
            $task->completed_on = Carbon::now();
            $task->save();
            return redirect()->back()->with(['success'=>'Task marked as Completed Successfully!']);
        }else{
            return Redirect::back()->withErrors('Task is already completed');
        }


    }
}
