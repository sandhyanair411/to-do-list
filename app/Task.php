<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //
    protected $fillable = ['name','description','user_id','is_complete','completed_on'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
