@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="card">
            @if (Auth::check())
                <div class="card-header">Tasks List</div>
                <div class="card-body">
                    <a href="{{route('add-task')}}" class="btn btn-primary">Add new Task</a>
                    <table class="table mt-4">
                        <thead><tr>
                            <th colspan="2">Tasks</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->tasks as $task)
                            <tr>
                                <td>
                                    {{$task->description}}
                                </td>
                                <td>
                                    <div content="col-md-12 ">
                                        <div class="col-md-3">
                                            <a class="btn btn-small btn-info" href="{{ URL::to('task/' . $task->id) }}">Edit</a>
                                        </div>
                                        <div class="col-md-3">
                                            <form class="delete" action="{{route('delete-task',$task->id)}}" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                {{ csrf_field() }}
                                                <input type="submit" value="Delete" class="btn btn-small btn-danger">
                                            </form>
                                            @if(!$task->is_complete)
                                            <form action="{{route('complete-task',$task->id)}}" method="POST">
                                                <input type="hidden" name="_method" value="PUT">
                                                {{ csrf_field() }}
                                                <input type="submit" value="Mark As Complete" class="btn btn-small btn-success">
                                            </form>
                                                @endif
                                        </div>
                                    </div>


                                </td>
                            </tr>


                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="card-body">
                    <h3>You need to log in. <a href="/login">Click here to login</a></h3>
                </div>
            @endif
        </div>
    </div>
@endsection