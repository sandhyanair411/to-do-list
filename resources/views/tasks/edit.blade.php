@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit the Task</h1>

        @include('tasks.form')

    </div>

@stop