<form action="@if($task) {{\Illuminate\Support\Facades\URL::to('task/'.$task->id)}} @else {{route('create-new-task')}}  @endif" method="post">
    <div class="form-group row">
        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Task Title') }}</label>

        <div class="col-md-6">
            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',($task)?$task->name:'') }}" required autocomplete="name" autofocus>

            @error('name')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Task Description') }}</label>
        <div class="col-md-6">
            <textarea name="description" class="form-control" required>{{ old('description',($task)?$task->description:'') }}</textarea>
            @error('description')
            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
            @enderror
        </div>

    </div>


    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                @if($task) Update Task @else Add Task @endif
            </button>
        </div>
    </div>
    {{ csrf_field() }}
</form>