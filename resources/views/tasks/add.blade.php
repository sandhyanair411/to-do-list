@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Add New Task</h2>

    @include('tasks.form')


</div>
@stop